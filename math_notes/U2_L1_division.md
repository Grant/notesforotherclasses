## A2.U2.L1 Division notes
### Key definitions/concepts
 1. Pretty much the same thing as multiplying. 
 2. Extra carfulness is needed (sqrt15 is not 15, it's sqrt15)
 3. There is a convention of mathamatics that says we are not done with example 1 at step 5. The conventions is that radicals should not be in the denominator, note because they can't be there, but because people don't like them there.
 4. One can check their work at each step in desmos.
 5. one number over the exact same number is equvilant to saying 1.
### Examples
 Example 1.
1. 7sqrt12x^3 / 28sqrt6x^4
2. (1)sqrt12x^3 / 4sqrt6x^4
3. sqrt2x^3 / 4sqrt(1)x^4
4. sqrt2 / 4sqrtx(^1)
5. sqrt2 / 4sqrtx
6. (sqrt2 / 4sqrtx) * sqrtx / sqrtx = sqrt2x / 4sqrtx^2 = sqrt2x / 4x
 Example 2.
1. 4y^2 / 4sqrt2x^2y^4 = y^2 / xy^2sqrt2
2. y^2 / xy^2sqrt2 = y^2sqrt2 / 2xy^2 = sqrt2 / 2x
### Summary
Division is done very simmilarly to multiplication. However, we need to be extra careful about sqrts. While there can be a radical in a denominator, because of a convetion, there shouldnt be. When dealing with that radical, we can multiply by a number over the same number, and that just multiplies by one.
