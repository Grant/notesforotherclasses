## Definitions/Key concepts
1. There is a pattern between radicans, exponents, and solved radicals that has to do with what kind of root is used and the fraction after it has been solved
2. This lets us preform opperations on more complex radicals when dealing with rational exponents
3. The goal for this unit is to let this concept sink in
4. Something is only raised to the power of the radican over the index if it is inside the root
5. Some things inside a root will have diferent radicans, so each of those will have a diferent exponent.
## Exapmples
- Example 1
- 2sqrtx^7 = 2x^(7 / 2)
- Example 2
- (cuberoot3y)^14 = (3y)^(14 / 3)
## Summary
When converting rationals into a format where the exponent is a fraction, the radican becomes the numerator and the index becomes the denominator. This is importent for complex operations later on, but not right now. It is also important to remember that something is only raised to the power of the radian over the index if it is inside the root, otherwise it is unafected by that exponent. If the contens of the root has multiple exponents for the diferent numbers/variables, then each of them will get a diferent fractional exponent.
