## A2.U2.L1 Addition notes
### Key definitions/concepts
 1. Addition, in this case, refers to addition and subtraction.
 2. To have like terms in addition, both the variables and exponents need to be the same.
 3. 5x + 3x does note = 8x^2, because we are adding x+x+x+x+x to x+x+x, so it is not squared, it is just 8x
 4.  3sqrt3 + 2sqrt3 does not = 5sqrt6 for the same reason 5x + 3x does not = 8x^2, it just = 5sqrt3
 5. There is a way to simplify terms that do not start as terms that cannot be simplified.
### Examples
 Example 1. 
1. 2xsqrt3x + 5sqrt3x^3
2. 2xsqrt3x + tsqrt(x^2 * sqrtx * sqrt3)
3. 2xsqrt3x + 5xsqrt3x
4. 7xsqrt3x
 Example 2.
1. 4sqrt3x + 5sqrt12x
2. 4sqrt3x + 5sqrt(4 * sqrt3 * sqrtx)
3. 4sqrt3 + 10sqrt3x
4. 14sqrt3x
### Summary
Subtraction is just adding a negative number, so this aplies for both addition and subtraction. To add things together algebreicly, both the variables and the exponents need to be the same. When this happens, th x or sqrt does not get squared, because ther is nothing that is squaring them, because this is addition. When there are terms that do not have the same variables and/or exponents, there is a way to make them addable.
