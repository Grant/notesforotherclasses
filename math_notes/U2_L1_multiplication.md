### Key definitions/concepts
 1. There is a significant relationship between multiplying algebreaic expressions and multiplying radicals.
 2. Radicals can be multiplyied as long as they have the same index.
 3. Multiplication is not the last step, there is still simplification.
 4. If there is nothing in front of a sqrt sign or a variable, then we can assume that is a 1, not a 0.
 5. There is a longer way to simplify, and that is by simplifying sqrts ahead of time.
### Examples
 Example 1. 
1. 4sqrt3x * 7sqrt6x^2 = 28sqrt18x^3
2. 28 * sqrt18 sqrtx^3
3. 28 * (sqrt9 * sqrt2) * (sqrtx^2 * sqrtx)
4. 28 * 3 * sqrt2 * x * sqrtx
5. 28 * 3 * x sqrt2 * x
6. 84x sqrt2x
 Example 2.
1. 1sqrt8 * 5sqrt2 = 5sqrt16
2. 5sqrt16 = 5 * 4
3. 5 * 4 = 20
### Summary
There is a conection in the multiplication of radicals and algebreaic expresions, and that is how the index is used. This is because radicals with the same index can be multiplied. However, multiplication is not the last step, as you still need to simplify. There is also a longer way to simplify by simplifying the sqrts before multiplication, but that way usualy takes longer.
