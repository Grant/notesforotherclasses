### Vocabulary
- Carbon
  - The building blocks for all life on earth
- Prokaryote 
  - A type of cell WITHOUT membrane-bound organells 
- Eukaryote
  - A type of cell WITH membrane-bound organells
- Animal Cell
  - The type of cell found in animals. It is eukaryotic cell WITHOUT a cell wall and chloroplast
- Plant Cell 
  - The type of cell found in plants. It is a eukaryotic cell WITH a cell wall and chloroplast
- Cell
  - The smallest form of life. All life functions hapen inside a cell.
- Tissue
  - Several cells form a tissue
- Organ
  - Several tissues form an organ
- Organ system
  - Several organs form an organ system
- Organism 
  - Several organ systems form an organism, owever, and organism can laso be made up of just cells, like unic-cellular organisms.
- Lipid Bilayer 
  - The outer layer of the cell membrane with water-repelling heads on the outside and water-attracting heads on the inside. 
- Selective permeability
  - A function of the cell membrane. It means that something only lets things pass through it SELECTIVLEY, so not everything can pass through it, but some things can.
- Diffusion 
  - The movement of molecules across the cell membrane going WITH the concentration gradient. These molecules are NONPOLAR and do not need a transport protien
- Facilitated Diffusion
  - The movement of molecules across the cell mkembrane going WITH the concentration gradient. These molecules are POLAR and need a transport protien.
- Primary active transport
  - The movement of a POLAR molecule or ion across the cell membrane going AGAINST the concentration gradient. These molecule need a transport protien, and use atp as energy.
- Secondary active transport
  - The movement of a POLAR molecule or ion across the cell membrane going AGAINST the concentration gradient. THese molecules need a transport protien, and uses a DIFERENT molecule undergoing Facilitated Diffusion at the same time as energy.
- Osmosis 
  - The movement of WATER across the cell membrane going WITH the concentration gradient. This NEEDS a special type of transport protien only for water, called an AQUAPORIN
- Stem Cells 
  - The base form of a cell (similar to a baby) before it becomes a specialized cell, like a blood cell or a bone cell.
- Interphase
  - NOT a part of mitosis. Is made up of by G0, G1, S, and G3.
- G0
  - Where cells are most of the time. The cell is not growing or preparing for mitosis, just doing what it normaliy does.
- G1
  - The cell grows to double the size and will increase cytoplasm
- S
  - The DNA is replicated
- G2
  - More growth, the cell prepares for cell division.
- M
  - Mitosis and Cytokinesis
- Prophase 
  1. DNA condenced into chromosones
  2. Nuclear envelope breaks down
  3. Centrioles begin to form spindle fibres
- Metaphase 
  1. Chromosones line up at the metaphase plate
  2. Spindle fibres attach to the centromeres
- Anaphase 
  - Spindle fibres retract and pull chromosones iwht them, so half of the chromosones are on each end of the cell.
- Telophase
  - The nuclear membrane reforms in both ends of the cell around the groups of chromosones, cells begin to divide cytoplasm
- Cytokinesis
  - The cells split
### Concepts
- Why is carbon important? 
- What are the six characteristics of living things? Provide at least one example of each. 
- Who are the three scientists that contributed to cell theory? 
- What evidence supported the development of cell theory? 
- What are the three parts of cell theory? 
- Why do some cells look different than other cells? 
- What organelles are present in plant cells, but NOT in animal cells? 
- What organelles are present in animal cells, but NOT in plant cells? 
- How do prokaryotes and eukaryotes work together to maintain life? Provide TWO examples. 
### Cell Transport Problems
1. There is a  small non-polar molecule, this molecule has a concentration of 7 molecules per mL in the cell and 12 molecules per mL in the environment. There is a transport protein present in the plasma membrane. ATP is not present. 
  - Will molecules move into the cell or out of the cell?
    - The molecules will move from the environment into the cell
  - What type of transport will this be? 
    - Diffusion
2. There is a large polar molecule that needs to be moved out of the cell. The concentration in the cell is 4 molecules per mL and the concentration outside the cell is 13 molecules per mL. A transport protein is present and ATP is present
  - What type of transport will this be? 
    - 
3. There is a polar molecule, the concentration is 26 molecules per mL in the cell and 18 molecules per mL in the environment. There is a transport protein present. ATP is present. 
  - Will molecules move into or out of the cell? 
  - What type of transport will this be? 

4. Molecule X is polar. Molecule X has a concentration of 19 molecules per L in the cell and 6 molecules per L in the environment. Molecule B is charged (it is an ion) and it has a concentration of 8 molecules per L in the cell and 36 molecules/L in the environment. Molecule X must be moved into the cell. There are transport proteins, there is no ATP. 
  - Where will molecule B move? 
  - What type of transport will this be? 

5. If molecule Z has a concentration of 27 molecules/L in the cell and 3 molecules/L in the environment, and molecule T has a concentration of 14 molecules/L in the cell and 2 molecules/L in the environment. If a cell has transport proteins can secondary active transport occur? 

6. Molecule C is a small non-polar molecule. The concentration in the cell is 8 molecules/L and the concentration in the environment is 17 molecules/L. There are no transport proteins or ATP. 
  - Where will molecule C move? 
  - What type of cell transport is this? 

7. Molecule L is an Ion. Molecule L has a concentration of 3 molecules/L in the environment and 12 molecules/L in the cell. There is a transport protein, but no ATP. Molecule L must move into the cell. 
  - What type of transport is this?  

