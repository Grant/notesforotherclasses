# Cell Theroy Day 1
## Notes: 
| Why is carbon inportant?                         | Because it is esentail for live on earth |
| ------------------------------------------------ | ------------------------- |
| What are the 6 charecteristics of living things? | They must respond to their environment (frog running away from dog), grow and develop(tadpoles becoming frogs), produce offspring(you know exactly what every teenager is thinking of), maintain homeostatsis(maintain a small range of internal temp), have complex chemistry(metabolism), and they have to consist of cells(they are microscopic). |
## Galery walk
### Station 1
    1. All grey-ish
    2. Small pimple-like dots
I think it is a Plant cell.
### Station 2
    3. Thick streak
    4. Veiny
I think it is a Skin cell.
### Station 3
    5. Very opaque purple streak and circle
    6.  Very incocestent
I think it is a Fat cell.
### Station 4
    7. Thin streak
    8. Small dots
I think it is a Bacteria cell.
### Station 5
    9. Looks kind of like a leaf
    10. Passageways
I think it is a Muscle cell.
## Notes
| Cell Theory |   |
| ----------- | - |
| What three people contributed to Cell Theory? | Theodor Schwan (plant cells), Matthias Jakob Schleiden (animal cells), Rudolf Virchow (Mitosis).|
| What are the three parts of cell theory? | 1. All organisms are made up of one or more cells, 2. All of the life functions of an organism occur within cells, 3. All cells come from preexisting cells. |
| Why are some cells shaped differently than others? | They have diferent functinos |
| Define Prokaryotic cell: | Cells without membrane bound organelles. |
| Define Eukaryotic Cell: | Cells with membrane bound organelles. |
## Higherarchy of Cells
| Cell | Example |
| ---- | ------- |
| Cells: Cells are the smallest unit of life and build all living organisms.| Ex. an animal cell |
| Tissue: Many cells make up one tissue. | Ex. Smooth muscle |
| Organ: Many tissues make up one organ. | Ex. Small Intestine |
| Organ system: Many organs make up an organ system. | Ex. Digestive system |
| Organism: Many organ systems make up an organsim. | Ex. Human |
