## Biochemestrey notes
### Metabolism & Enzymes

Metabolism: All the biochemical reations that occur within the body to maintain life. Sometimes, these reactions break down molecules, while other times it combines smaller molecules into larger ones. All of these are important for maintaining homiostasis.

Homiostasis is the maintenance of stable conditions within an organism. To maintain stable tempatures, we shiver to get hotter and sweat to get cooler.



Enzymes: Proteins produced by our bodies that catalyze (cause to go faster) reactions. They allow biochemical reactions in our bodies to hapen efficiently. The 2 that we need to know is Lipase, which helps break down lipids in the small intestine, and Pepsin, which helps break down protiens in the stomach.

Biochemical reactions are shown with a graph. The amount of free energy is G, or the y-axis. The x-axis is the reaction time. Reactants are at the far left of the graph, or the y-intercept, and are the ingredients of the reaction. The relitive maximum is the Transition state, and this is before the ingredients have chemicly bonded. However, they bond in the time between the transitino state and the final products, which is at the end of the graph. The diference between the free enegy of the reactants and the transition state is called the activation energy, and it is the energyt state needed for a biochemical reaction to hapen.
Enzymes direcley affect biochemical reactions by extremley reducing the activation energy.

### The conection
Enzymes make metabolism more efficient. They increas the rate of reacinos by LOWERING the activation energy. They make reaction happen faster. 

How it works: The substrate (ingredients) atach to the enzyme, forming an enzyme-substrate complex. After the reaction occurs, the product/s leave the enzyme. The part where the substrate goes into the enzyme is called the active site, and it is very important that the enzyme polds correctly so that the substrate can fit into the active site. This is because enzymes are protiens, meaning that how their invironment and how they fold is very important for the enzyme to be able to aid in the biochemical reaction.

### Graph
![alt text](photos/IMG_2582.HEIC "Graph")

