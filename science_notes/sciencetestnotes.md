## Science test 1 notes
### Scientific method
Hypothesis: An educated guess about the outcome, generaly in the if then because format, and no i think.
Observation: Something you see/smell/taste/feel/hear that can be qualitative or quantitative.
Analysis: An explation of what results mean.
Results: Data from an experiment, often a graph or table.
Conclusion: What happened, the awnser to the question
Question: A testable question that you can do an experiment to find an awnser
Procedure: The steps of the experiment
Experiment: Solving the question
Materials: Stuff you need to do for the experiment.
### Germ theory
Robert Koch: Made Koch's postulates, part of germ theory
Koch's postulates:
1. You need to find a sick person and be able to tell that they have symptoms of the disease
2. You have to be able to take the bacterie from the sick person.
3. You have to be able to take the bacteria from a sick person and give it to a healty person
4. The bacteria from the firts person has to be identical to the bacteria in the second person.
### Viral replication
1. DNA or RNA(virus) enters a host cell
2. Virus ataches itself to host DNA
3. Virus makes an exact copy of itself using host machinery.
4. Eject replica virus from host cell
5. Replica virus goes back to step 1
### Bacteria and Viruses
Bacteria are not directly harmful, they make toxins that hurt you.
A virus does not directly hurt the host, they make protiens that hurt you.
Antibiotics kill bacteria.
Vaccines prevent viruses by making antibodies which fight (not kill) the virus.
A pathogen is a bad bacteria or any virus.
The streak plate iscolates bacteria
Culture broth is what bacteria is grown in.
## Study guide
### Vocabulary
● Antibiotic
    Kills Bacteria 
● DNA
    Genetic material that is a nucleaic acid
● RNA
    Genetic matherial that is a nucleaic acid
● Double helix
    The structure of DNA 
● Antibody
    Produced by vaccines to fight (BUT NOT KILL) viruses
● Nucleic Acid 
    Genetic material
● Bacteria 
    A prokaryotic organisim that can be good or bad, survive in almost any environment, and use Binary Fission for reproduction.
● Adaptation
    When an organisim changes without evolution to survive better in an envornment
● Virus 
    A bad or nuetral piece or DNA or RNA that infects a host cell and forces it to make protiens so that the peice of DNA or RNA can replicate itself using host machinery
● Pathogen 
    Any virus or a bad bacteria.
● Vaccine 
    An ingection that helps figh viruses by making anitbodies.
● Plasma membrane
    Keeps oragnells in place
● Nucleus 
    The part of the cell that holds the DNA of the cell
● Eukaryote
    A multicelled organism 
● Binary fission
    The process in which a bacteria reproduces that is similar to mitosis
● Replication
    Creating an exact copy of oneself to reproduce.
● Cell wall 
    The outer part of a cell found in all plant cells and some prokaryotic cells.
● Prokaryote 
    A single-celled organisim without a nucleus
● Unicellular 
    Made of one cell
● Antibiotic resistance
    What happenes when too many antibiotics are released into the environment and antibiotics become less and less efective 
● Bacterial culture
    What is used to grow bacteria coninies.
● Streak plate method 
    The method in which one issolates bacteria using a streak plate.
● Toxins 
    Produced by harmful bacteria that are what harms you when a bad bacteria infects you
● Proteins 
    Produced by cells infected by a virus. They are what harms you, not the virus itself.
● Germ Theory 
    A theory on weather or not someones disease is caused by a bacteria using Koch's postulates:
    The postulates:
        1. You need to find a sick person.
        2. You need to be able to remove the bacteria from the sick person.
        3. You need to be able to give a healthy person the bacteria.
        4. You need to observe the same symptoms in both people.
● Host 
    Something infected by an infection that is used for the infection's pourposes
● Host Machinery 
    The systems of a cell that get hyjacked by a virus.
● Robert Koch 
    The person who figured out Koch's postulates
● Kirby-Bauer disk diffusion assay
    The method used to test antibiotic efectivness aginst a bacteria.
● The scientific method 
    A method that is used to answer scientific questions.
        ○ Observation 
            Something that you see/smell/hear/taste/feel, can be quantitative or qualitative
        ○ Scientific question
            A question that can be awnsered with an experiment
        ○ Hypothesis 
            A prediction about the results of an experiment formated as a statement
        ○ Procedure 
            What is done in an experiment
        ○ Results 
            What happens in an experiment, usualy formated as a graph, chart, or table
        ○ Analysis
            The meaning of the results
        ○ Conclusion
            What happend, formated as an awnser to the question
## Concepts
● How did antibiotics change the way doctors treated patients? 
    -Antibiotics let doctors treat patients by getting rid of the infectious bacteria instead of managing symptoms.
● How did viruses change the way doctors treated patients? 
    -They doctors to administer vaccines before the virus was caught so that patients would not get viruses or would not get as affectedx by firuses.
● How do bacteria affect humans?
    -They can affect humans in good ways and bad ways.
        ○ Good ways? 
            -Helps with the digestive system, immune system, vitamin production, ect
        ○ Bad ways? 
            -Producing toxins, which are harmful to humans.
● How do viruses affect humans? 
    -Viruses are never good to humans. They can either do nothing to humans, or they can be harful to humans by producing protiens.
● What do antibiotics do? 
    -Antibiotics kill bacteria.
        ○ Why are antibiotics good?
            -They let doctors treat patients with bacteria instead of managing symptoms 
        ○ Why are antibiotics bad? 
            -They kill good bacteria if used incorectly, and can lead to antibiotic ressistance.
● What do vaccines do? 
    -They help prevevnt and fight viruses.
        ○ Specifically, what do vaccines help your body create? And why are these “things” important? 
            -Vaccines produce antibodies, which are important because they fight (BUT NOT KILL) viruses.
● What makes an observation different from an idea? 
    -An observatino is an indisputab le fact about the environment, while ideas are formed based on observations and are not indisputable facts, but rather inferences based on the environment.
● What makes a scientific question different from a regular question?  
    -A scienctific questino is able to be tested with an experiment.
● Explain why viruses and bad bacteria are not what DIRECTLY cause symptoms in people. 
    -The symptoms caused by these pathogens are not cause by the pathogen being there. They are caused by what the pathogen makes/forces your body to make. Viruses force the host cell to produce protiens, which are harmful to humans. Bad bacteria produce toxins, which are harmful to the human body.
● What are Koch’s postulates? What did they contribute to? 
    -Koch's postulates are a way to discover what kind of bacteria is affecting a patient.
        ○ Draw out koch’s postulates 
            1. You need to find a sick person.
            2. You need to be able to remove the bacteria from the sick person.
            3. You need to be able to give a healthy person the bacteria.
            4. You need to observe the same symptoms in both people.
● Tell me a story about how a bacteria or virus could spread. 
    -Barry gets infected by a bad bacteria that was on a dog that he pet on his way to a doctors apointment. However, because he thinks he is healthy he sits in the healthy waiting room, infecting everyone there. As it turns out, one of his co-workers is waiting for her doctors apointment as well, and get sick. A week later, she comes in contact with Barry before taking antibiotics, because symptoms had not manifested. At this point, Barry had goten rid of the bacteria, however, he gets infected by his co-worker.
● Draw and label a bacteria and a virus. 

● Draw binary fission and viral replication and explain the steps of these processes. 
● How do antibiotics cure infections? Hint: TWO ways! 
    -They kill bacteria.
    -They prevent bacteria from spreading.
● What might happen if you don’t take antibiotics for all the days they have been prescribed for? 
    -The firs wave will kill off the weak bacteria but will leave the strong bacteria, which will then multiply and are harder to be cured.
● What can happen when too many antibiotics are prescribed and therefore released into the environment?
    -It will lead to antibiotic resistance in the envornment, and will kill helpful bacteria in the enviornment.
● What is wrong with prescribing antibiotics when there is no evidence of a current infection? 
● What is wrong with feeding livestock antibiotics?
    -It leads to antibiotic ressistance in the environment and creates a paradox where the livestock need to be fed the antibiotics so that they don't get sick from the antibiotic ressistant bacteria. 
● What are some alternative options that might help with the antibiotic crisis?
    - 
● What are the three most common shapes of bacteria? 
    -Cocci, Bacilli, and Spirilla.
● What are the two types of viruses? 
    DNA and RNA.
● Why is it important to have a precise procedure?
    -So that the procedure is very accurate and can be recreated and re-tested by others. 
● Why is the streak plate method important? 
    It helps issolate difernt types of bacteria and helps doctors discover what kind of bacteria is affecting the patient and what antibiotics should be perscribed.
● When is a bacterial lawn useful? What makes it different from a streak plate?
    - 
● How can we test for antibiotic resistance? 
    Kirby-Bauer disk diffusion assay, which lets a scientest see how efective diferent antibiotics are against one type of bacteria.
