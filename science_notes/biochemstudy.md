## Terms/vocabulary

1. Atom
What everything is made up of.
2. Molecule
A group of atoms making something
3. Chemical bond
An extremely strong bond that is hard to break because of a chemical reaction that caused the bond
4. Electronegativity
How atractive or repelent an atom is of electrons when in a molocule
5. Polar Covalent Bonds
Bonds between atoms that are polar because of the electronegitivity of atoms.
6. Non-Polar Covalent Bonds
A bond between atoms that does not use electronegitivity.
7. Hydrogen Bonds
Bonds between the hydrogen atoms of one molocule and oxygen atoms of another water molocule
8. Adhesion
What happens when water molecules stick to polar non-water molocules
9. Cohesion
When water molocules stick to other water molocules
10. Capillary Action
Where water uses adhesion to stick to the inside of a plants stem and water climbs up each other doing this using cohesion
11. Surface Tension
The thin layer on the surface of water that can hold things up because of cohesion
12. Specific Heat
The amount of energy needed to change something's tempature
13. Polymer
A thing that is made up of many parts, many macromolecules are polymers.
14. Monomer
The things that make up polymers
15. Lipid
Not made up of monomers, store energy, make up structural components of the cell, and most importantly acts as chemical messengers around the body
16. Carbohydrate
A type of macromolecule that is a polymer made up of monosaccharides that serves to store energy and make up cell walls.
17. Protein
A type of marcomolecule that is a polymer made up of amino acids that has many functions, but most importantsly helps molecules cross the plasma membrane (channel protien)
18. Nucleic Acid
A type of marcomolecule that is a polymer that is made up of Nucleotides. They store genetic information and contain the code to build protiens.
19. Amino Acid
The Monomers of Protien
20. Monosaccharide
the monomers of carbohydrates
21. Polysaccharide
another name for carbohydrates
22. Nucleotide
the monomers of nucleic acid
23. Enzyme
A protien produce by our body that can make biochemical reactions more efficient by reducing the activation energy.
24. Reactants
The things that are put into a reaction to make something
25. Transition state
The threshold of the reaction, the peak of the graph
26. Activation energy
The amount of energy required ofr the change to take place
27. G(Y-axis label)
Free energy
28. Biochemical reaction
A transformation from one type of molecule into a diferent type of molecule in a cell
29. Metabolism
All the biochemical reactions in the body, helps maintain homiostasis
30. Homeostasis
A state of stability in living organisims (tempature)
31. Substrate
The reactant/reactants
32. Products
The thing/things created by the reaction
33. Active Site
The spot in the enzyme that the substrate/s go into that lets them react with a much lower activation energy

## Concepts

1. What does every living thing need?
Oxygen, space, water
2. Why is it important that water can dissolve many things?
It transports minerals and nutreints throughout the enviornment
3. Why is it important that water has a high heat capacity?
It provides a source of stability for organisms living in the water, no matter how hot or cold it is
4. Why is it important that water has surface tension?
It lets some organisms walk on the water, like water striders
5. Why is cohesion important?
Capilary action, surface tension
6. Why is adhesion important?
Capilary action
7. How does cohesion contribute to surface tension?
It makes the water at the top more dense by sticking together
8. How do cohesion and adhesion contribute to capillary action?
Adhesion lets water stick to the sides of the stem, while cohesion lets water climb on top of each other
9. What is the difference between cohesion and adhesion?
Cohesion is sticking to each other, while adhesion is sticking to other things
10. How many atoms are in a water molecule? Which ones?
There are 2 kinds of atoms and a total of 3 atoms. There is 1 oxygen atom and 2 hydrogen atoms
11. Draw three water molecules connected to each other. Label the atoms, charges, and ALL bonds.
![Water Drawing](photos/IMG_2589.HEIC)
12. How to read the table below?
![Periodic Table of Elements Electronegitivity](photos/Screen Shot 2022-10-14 at 9.07.18 AM Small.jpeg)
The more red an element is, the more electronegitive it is
13. What makes up the majority of a cell?
Water
14. What are the names of the four macromolecules/Biomolecules?
Proteins, carbohydrates, nucleic acids, and lipids
15. What can the structure of something  (IN BIOLOGY)  tell us about it?
It can tell us it's function.
16. Choose one item you use in your daily life that has a particular structure to carry out a particular function. Explain it below.
A water bottle has a spesific structure to hold water without leaking, but let out water for us to drink when we use it.
17. Review the functions, monomers, structure, and examples of the 4 macromolecules using the macromolecules review worksheet from last class.
| Macromolecule | Polymer? | Monomer?        | Function   | Example Molecule |
| ------------- | -------- | --------------- | ---------- | ---------------- |
| Protien       | Yes      | Amino Acids     | Membrane   | Channel Protien  |
| Carbohydrate  | Yes      | Monosaccharides | Storage    | cellulose        |
| Nucleic Acid  | Yes      | Nucleotides     | Gene store | DNA              |
| Lipids        | No       | No              | Chemmesngr | testoterone      |
18. Review the water chemistry review on the macromolecules worksheet.
Water molecules have polar covalent bonds. This is because the oxygen atom is more electronegitive than the hydrogen atoms. This results in the oxygen atom having a partial negitive charge and the hydrogen atoms having a partial positive charge. Water molecules bond to other water molecules through hydrogen bonds. These types of bonds connect the hydrogen atom on one water molecule to the oxygen atom on another water molecule.
19. How do enzymes increase the rate of the reaction?
They reduce the activation energy
20. Do enzyme reactions make 2 substrates 1 product?  or make 1 substrate 2 products? Or can they do both?
They can use multible substrates to make one product, or use one substrate to make multiple products
21. Draw a free energy graph showing a chemical reaction, add a line showing the
same reaction with an enzyme added.
![Reaction Graph](photos/IMG_2588.HEIC)
