## Definitions
- DNA
  * Deoxynucleic Acid. It is used to make mRNA. It is made up of A, T, G, C. A pairs up with T, and G pairs up with C.
- Nucleotides 
  * The molecules that make up DNA and RNA. They are A, T, U, G, and C
- ATGC
  * The four bases/nucleiotides of DNA.
- AUGC
  * The four bases/nucleiotides of RNA.
- Double helix 
  * The structure of DNA
- Single helix 
  * The structure of RNA
- Non-coding strand 
  * The strand of DNA that IS NOT used to make RNA.
- Coding strand 
  * The strand of DNA that IS used to make RNA.
- mRNA
  * A copy of the coding strand of DNA that is used to make protiens.
- Transcription
  * The proces where mRNA is made based on the coding strand of DNA.
- Translation
  * The process where mRNA is used to make a protien.
- Nucleus 
  * The center of a molecule. It holds DNA.
- Ribosome
- Protein 
  * Made by mRNA. Caries out functions based on what protien is made.
- Amino Acid 
  * The molecules that make up protiens.
- Codon
- tRNA 
  * The RNA that brings Amino Acids to the mRNA to make a protien
- The Central Dogma 
- Gene 
  * What determines the physical charecteristics of someone.
- Allele
  * A single gene that helps determine what the final result is.
- Dominant 
  * Will always show if it is there.
- Recessive 
  * Will only show if there are no Dominants.
- Punnett Square 
  * A chart that is used to find the posibility of a single chartaristic of the offspring of parents.
- Genotype 
  * Determined by both allels, determines phenotype.
- Phenotype 
  * The physical characteristic as a result of the genotype.
- Homozygous 
  * Both allels same.
- Heterozygous 
  * Both allels diferent.
- Genetics 
- Dihybrid cross 
  * Similar to a punnet square, but used to determine the probability of multipule characteristics.
- Meiosis 
  * The process where a cell splits twice to make 4 sperm or egg cells. These cells are left with 23
- Meiosis 1 
  * The initial division in meiosis.
- Meiosis 2
  * The second division of meiosis.
- Crossing Over
- Principles of biomedical ethics 
- Synthetic Biology
## Concepts
- How many different genes can you compare in one  punnett square? 
  * 4
- How many different genes can you compare in a dihybrid cross? 
  * infinite
- Who are the four scientists who contributed to the structure of DNA we have today? 

- Where does transcription take place? 
  * The nucleus
- Where does translation take place? 

- What are the key differences between mitosis and meiosis? 
  * Mitosis is where a cell divides  to make 2 perfect replicas, while meiosis makes 4 diferent variations.
- In what phase of meiosis does crossing over occur? How does crossing over increase genetic diversity?

- In at least 5 sentences make an argument for OR against the use of synthetic biology on living organisms? 
## Practice Problems
1. Draw out a DNA molecule. 

2. Draw out a mRNA molecule. 

3. Convert the following coding strand to mRNA:
  5. TTAGCCATATGCATGCGCCCATTGAT 3.

5. UUAGCCAUAUGCAUGCGCCCAUUGAU 3.

4. Convert the following mRNA to a non-coding strand:
  5. CUAACUGGCAUCAGUAGCAACGUA 3.

3. GATTGACCGTAGTCATCGTTGCAT 3.

5. Convert the following non-coding strand into a protein
  3. TACGTACGATACGATAGCTACGATAA 5.

  

6. List out the three possible genotypes for one gene. Next to each genotype write the phenotype.

7. Complete a punnett square for the following individuals for the gene: height. What percent of the offspring will be short?
  - Gene: Height  H=Tall h=short
  - P1: Hh
  - P2: hh
  ---------------
  |   | H  | h  |
  ---------------
  | h | Hh | hh |
  ---------------
  | h | Hh | hh |
  ---------------
  * 50%
8. Find the gametes for the following individuals
  - H gene= Height H=Tall h=Short
  - B gene= Eyes B=Brown b=blue
    - P1: Hh,Bb
    - P2: Hh, bb
  * P1: HB, Hb, hB, hb
  * P2: Hb, hb
9. Complete a dihybrid cross using the gametes from question 8
  ----------------------------------------
  | Gametes |  HB  |  Hb  |  hB  |  hb  |
  ----------------------------------------
  |   Hb    | HHBb | HHbb | HhBb | Hhbb |
  ----------------------------------------
  |   hb    | HhBb | Hhbb | hhBb | hhbb |
  ----------------------------------------

10. What percent of offspring in the dihybrid cross on question 9 are tall AND have blue eyes?
  - 37.5%
